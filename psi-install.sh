#!/bin/bash
INSTALL_DIR='/opt/philips-saeco-interface'
LOGROTATE_DIR='/etc/logrotate.d'
GIT_REPO_PATH='git@bitbucket.org:dflasch/philips-saeco-interface.git'
USER='pi'
GROUP='pi'
RED='\033[0;31m'
NC='\033[0m'

function remove_system_service {
	echo -e "${RED}...removing system service${NC}"
	sudo systemctl disable philips-saeco-interface.service
	sudo systemctl stop philips-saeco-interface.service
	sudo rm /etc/systemd/system/philips-saeco-interface.service
	sudo systemctl daemon-reload
}

function download_and_install {
	echo -e "${RED}...downloading sourcecode${NC}"
	git clone $GIT_REPO_PATH $INSTALL_DIR
	echo -e "${RED}...configure npm${NC}"
	npm install --prefix $INSTALL_DIR $INSTALL_DIR
}

function configure_as_service {
	echo -e "${RED}...grant node root rights for BLE Module${NC}"
	sudo setcap cap_net_raw+eip $(eval readlink -f `which node`)
	echo -e "${RED}...configure as a service${NC}"
	sudo mv $INSTALL_DIR/philips-saeco-interface.service /etc/systemd/system/philips-saeco-interface.service
	sudo systemctl enable philips-saeco-interface.service
	sudo systemctl start philips-saeco-interface.service
}

function create_install_directory {
	echo -e "${RED}...create install directory${NC}"
	sudo mkdir $INSTALL_DIR
	sudo chown $USER:$GROUP $INSTALL_DIR
}

function remove_install_directory {
	echo -e "${RED}...remove install directory${NC}"
	sudo rm -rf $INSTALL_DIR
}

function create_logrotate_daemon {
	echo -e "${RED}...create logrotate daemon${NC}"
	sudo touch "${LOGROTATE_DIR}/philips-saeco-interface"
	sudo echo "${INSTALL_DIR}/*.log {" >> /etc/logrotate.d/philips-saeco-interface
	sudo echo "size 1024k" >> /etc/logrotate.d/philips-saeco-interface
	sudo echo "missingok" >> /etc/logrotate.d/philips-saeco-interface
	sudo echo "rotate 3" >> /etc/logrotate.d/philips-saeco-interface
	sudo echo "compress" >> /etc/logrotate.d/philips-saeco-interface
	sudo echo "copytruncate" >> /etc/logrotate.d/philips-saeco-interface
	sudo echo "}" >> /etc/logrotate.d/philips-saeco-interface
}

function remove_logrotate_daemon {
	echo -e "${RED}...remove logrotate daemon${NC}"
	sudo rm "${LOGROTATE_DIR}/philips-saeco-interface"
}

case "$1" in
	install)
		if [ -d $INSTALL_DIR ]; then
			echo $"philips saeco interface is already installed! Run '$0 reinstall' or '$0 uninstall'." && exit 1
		fi

		if [ $# -eq 2 ];then
    	GIT_REPO_PATH=$2;
    	echo $GIT_REPO_PATH;
		fi

		create_install_directory
		create_logrotate_daemon
		download_and_install
		configure_as_service
		;;

	reinstall)
		if [ ! -d $INSTALL_DIR ]; then
			echo $"philips saeco interface is not installed yet! Run '$0 install' first." && exit 1
		fi

		if [ $# -eq 2 ];then
    	GIT_REPO_PATH=$2;
    	echo $GIT_REPO_PATH;
		fi

		remove_system_service
		remove_install_directory
		remove_logrotate_daemon
		create_install_directory
		create_logrotate_daemon
		download_and_install
		configure_as_service
		;;

	uninstall)
		if [ ! -d $INSTALL_DIR ]; then
			echo $"philips saeco interface is not installed yet!" && exit 1
		fi
		remove_install_directory
		remove_logrotate_daemon
		remove_system_service
		;;

	*)
		echo $"Usage: $0 install|reinstall|uninstall"
		echo ""
		echo $"install [path-to-source] - Path is optionial. If not supplied, I'll load the source from bitbucket."
		echo $"                           Install directory will be $INSTALL_DIR."
		echo ""
		echo $"reinstall [path-to-source]  - Path is optionial. If not supplied, I'll load the source from bitbucket."
		echo ""
		echo $"uninstall"
		echo ""
		exit 1

esac