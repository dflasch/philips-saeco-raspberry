# Raspberry einrichten

## Raspian auf Deutsch umstellen
Zuerst sollte die Sprache und Tastaturbelegung auf Deutsch umgestellt werden
```
sudo raspi-config
sudo reboot
```

## Wifi einrichten
```
vi nano /etc/wpa_supplicant/wpa_supplicant.conf
```
am Ende der Datei das Wlan mit SSID und PSK konfigurieren
```
network={
    ssid="The_ESSID_from_earlier"
    psk="Your_wifi_password"
}
```

# Node und Git installieren
## node.js installieren
```
curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash 
sudo apt-get install -y nodejs
```
## git installieren
```
sudo apt-get install git
```

# Die Software philips-saeco-interface installieren
Die Software erlaubt den Fernzugriff auf die Kaffeemaschine

## Einen SSH Schlüssel generieren 
Durch den Schlüssel kann das private Repository ohne die Angabe eines Passworts heruntergeladen werden
```
ssh-keygen -t rsa -b 4096 -C "your_email@example.com"
```
Den Schlüssel dann, wie hier beschrieben: https://confluence.atlassian.com/bitbucket/add-an-ssh-key-to-an-account-302811853.html
zum Repository hinzufügen.

## Die Software installieren/deinstallieren/neu installieren
```
cd ~
curl -H 'Cache-Control: no-cache' https://bitbucket.org/dflasch/philips-saeco-raspberry/raw/master/psi-install.sh > psi-install.sh && chmod +x psi-install.sh
./psi-install.sh install|uninstall|reinstall
```

## Den Daemon für die Software starten/beenden
```
sudo systemctl start|stop philips-saeco-interface
```

## Die Software beim Bootvorgang starten
```
sudo systemctl enable philips-saeco-interface
```